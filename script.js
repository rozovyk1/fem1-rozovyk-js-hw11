const buttons = document.querySelectorAll(".btn");
document.onkeypress = function (event){
    buttons.forEach(function (element) {
        if(event.key.toUpperCase() === element.textContent.toUpperCase()) {
            element.style.backgroundColor = "blue";
        }
        else {
            element.style.backgroundColor = "black";
        }
    });
};
